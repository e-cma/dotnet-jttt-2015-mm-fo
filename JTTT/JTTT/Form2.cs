﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT
{
    public partial class Form2 : Form
    {

        public Form2(string HTML)
        {
            InitializeComponent();
            webBrowser1.DocumentText = HTML;
            webBrowser1.Refresh();
            this.Size = webBrowser1.Size;
            this.Refresh();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            this.Size = webBrowser1.Size;
            this.Refresh();
        }
    }
}
