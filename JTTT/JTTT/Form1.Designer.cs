﻿namespace JTTT
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.nazwa = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.strona_tag = new System.Windows.Forms.TextBox();
            this.strona_adres = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.mail_adres = new System.Windows.Forms.TextBox();
            this.Wykonaj = new System.Windows.Forms.Button();
            this.wczytaj = new System.Windows.Forms.Button();
            this.zapisz = new System.Windows.Forms.Button();
            this.dodawanie = new System.Windows.Forms.Button();
            this.lista = new System.Windows.Forms.ListBox();
            this.usuń = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.web = new System.Windows.Forms.TabPage();
            this.api = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.mail = new System.Windows.Forms.TabPage();
            this.okno = new System.Windows.Forms.TabPage();
            this.miasto = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.web.SuspendLayout();
            this.api.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.mail.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(39, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "nazwa";
            // 
            // nazwa
            // 
            this.nazwa.Location = new System.Drawing.Point(83, 12);
            this.nazwa.Name = "nazwa";
            this.nazwa.Size = new System.Drawing.Size(203, 20);
            this.nazwa.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "tag";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "adres";
            // 
            // strona_tag
            // 
            this.strona_tag.Location = new System.Drawing.Point(67, 44);
            this.strona_tag.Name = "strona_tag";
            this.strona_tag.Size = new System.Drawing.Size(203, 20);
            this.strona_tag.TabIndex = 1;
            // 
            // strona_adres
            // 
            this.strona_adres.Location = new System.Drawing.Point(67, 18);
            this.strona_adres.Name = "strona_adres";
            this.strona_adres.Size = new System.Drawing.Size(203, 20);
            this.strona_adres.TabIndex = 0;
            this.strona_adres.Text = "http:\\\\demotywatory.pl";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "adres";
            // 
            // mail_adres
            // 
            this.mail_adres.Location = new System.Drawing.Point(67, 16);
            this.mail_adres.Name = "mail_adres";
            this.mail_adres.Size = new System.Drawing.Size(203, 20);
            this.mail_adres.TabIndex = 2;
            // 
            // Wykonaj
            // 
            this.Wykonaj.Location = new System.Drawing.Point(626, 301);
            this.Wykonaj.Name = "Wykonaj";
            this.Wykonaj.Size = new System.Drawing.Size(90, 31);
            this.Wykonaj.TabIndex = 2;
            this.Wykonaj.Text = "Wykonaj";
            this.Wykonaj.UseVisualStyleBackColor = true;
            this.Wykonaj.Click += new System.EventHandler(this.Wykonaj_Click);
            // 
            // wczytaj
            // 
            this.wczytaj.Location = new System.Drawing.Point(457, 301);
            this.wczytaj.Name = "wczytaj";
            this.wczytaj.Size = new System.Drawing.Size(75, 31);
            this.wczytaj.TabIndex = 3;
            this.wczytaj.Text = "wczytaj";
            this.wczytaj.UseVisualStyleBackColor = true;
            this.wczytaj.Click += new System.EventHandler(this.wczytaj_Click);
            // 
            // zapisz
            // 
            this.zapisz.Location = new System.Drawing.Point(376, 301);
            this.zapisz.Name = "zapisz";
            this.zapisz.Size = new System.Drawing.Size(75, 31);
            this.zapisz.TabIndex = 4;
            this.zapisz.Text = "zapisz";
            this.zapisz.UseVisualStyleBackColor = true;
            this.zapisz.Click += new System.EventHandler(this.zapisz_Click);
            // 
            // dodawanie
            // 
            this.dodawanie.Location = new System.Drawing.Point(30, 280);
            this.dodawanie.Name = "dodawanie";
            this.dodawanie.Size = new System.Drawing.Size(296, 37);
            this.dodawanie.TabIndex = 5;
            this.dodawanie.Text = "dodaj do listy";
            this.dodawanie.UseVisualStyleBackColor = true;
            this.dodawanie.Click += new System.EventHandler(this.dodawanie_Click);
            // 
            // lista
            // 
            this.lista.FormattingEnabled = true;
            this.lista.Location = new System.Drawing.Point(376, 34);
            this.lista.Name = "lista";
            this.lista.Size = new System.Drawing.Size(340, 238);
            this.lista.TabIndex = 6;
            // 
            // usuń
            // 
            this.usuń.Location = new System.Drawing.Point(538, 301);
            this.usuń.Name = "usuń";
            this.usuń.Size = new System.Drawing.Size(75, 31);
            this.usuń.TabIndex = 7;
            this.usuń.Text = "usuń";
            this.usuń.UseVisualStyleBackColor = true;
            this.usuń.Click += new System.EventHandler(this.usuń_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.web);
            this.tabControl1.Controls.Add(this.api);
            this.tabControl1.Location = new System.Drawing.Point(37, 49);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(289, 99);
            this.tabControl1.TabIndex = 8;
            // 
            // web
            // 
            this.web.Controls.Add(this.strona_adres);
            this.web.Controls.Add(this.strona_tag);
            this.web.Controls.Add(this.label2);
            this.web.Controls.Add(this.label1);
            this.web.Location = new System.Drawing.Point(4, 22);
            this.web.Name = "web";
            this.web.Padding = new System.Windows.Forms.Padding(3);
            this.web.Size = new System.Drawing.Size(281, 73);
            this.web.TabIndex = 0;
            this.web.Text = "Strona www";
            this.web.UseVisualStyleBackColor = true;
            // 
            // api
            // 
            this.api.Controls.Add(this.label5);
            this.api.Controls.Add(this.miasto);
            this.api.Location = new System.Drawing.Point(4, 22);
            this.api.Name = "api";
            this.api.Padding = new System.Windows.Forms.Padding(3);
            this.api.Size = new System.Drawing.Size(281, 73);
            this.api.TabIndex = 1;
            this.api.Text = "openweather";
            this.api.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.mail);
            this.tabControl2.Controls.Add(this.okno);
            this.tabControl2.Location = new System.Drawing.Point(37, 154);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(285, 100);
            this.tabControl2.TabIndex = 9;
            // 
            // mail
            // 
            this.mail.Controls.Add(this.label3);
            this.mail.Controls.Add(this.mail_adres);
            this.mail.Location = new System.Drawing.Point(4, 22);
            this.mail.Name = "mail";
            this.mail.Padding = new System.Windows.Forms.Padding(3);
            this.mail.Size = new System.Drawing.Size(277, 74);
            this.mail.TabIndex = 0;
            this.mail.Text = "mail";
            this.mail.UseVisualStyleBackColor = true;
            // 
            // okno
            // 
            this.okno.Location = new System.Drawing.Point(4, 22);
            this.okno.Name = "okno";
            this.okno.Padding = new System.Windows.Forms.Padding(3);
            this.okno.Size = new System.Drawing.Size(277, 74);
            this.okno.TabIndex = 1;
            this.okno.Text = "okno";
            this.okno.UseVisualStyleBackColor = true;
            // 
            // miasto
            // 
            this.miasto.Location = new System.Drawing.Point(67, 6);
            this.miasto.Name = "miasto";
            this.miasto.Size = new System.Drawing.Size(203, 20);
            this.miasto.TabIndex = 0;
            this.miasto.Text = "Wroclaw";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Miasto";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(728, 351);
            this.Controls.Add(this.tabControl2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.usuń);
            this.Controls.Add(this.nazwa);
            this.Controls.Add(this.lista);
            this.Controls.Add(this.dodawanie);
            this.Controls.Add(this.zapisz);
            this.Controls.Add(this.wczytaj);
            this.Controls.Add(this.Wykonaj);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.zapisz_Click);
            this.Load += new System.EventHandler(this.wczytaj_Click);
            this.tabControl1.ResumeLayout(false);
            this.web.ResumeLayout(false);
            this.web.PerformLayout();
            this.api.ResumeLayout(false);
            this.api.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.mail.ResumeLayout(false);
            this.mail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox strona_tag;
        private System.Windows.Forms.TextBox strona_adres;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox mail_adres;
        private System.Windows.Forms.Button Wykonaj;
        private System.Windows.Forms.Button wczytaj;
        private System.Windows.Forms.Button zapisz;
        private System.Windows.Forms.Button dodawanie;
        private System.Windows.Forms.ListBox lista;
        private System.Windows.Forms.TextBox nazwa;
        private System.Windows.Forms.Button usuń;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage web;
        private System.Windows.Forms.TabPage api;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox miasto;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage mail;
        private System.Windows.Forms.TabPage okno;
    }
}

