﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using HtmlAgilityPack;
using Newtonsoft.Json;



namespace JTTT
{
    //[Serializable]

    public class Data
    {

        public int Id { get; set; }
        public string nazwa { get; set; }
        //public Tasc wykonanie { get; set; }
        public bool email { get; set; }
        public bool okno { get; set; }

        public string mail_adress { get; set; }
        public virtual void wykonaj() {}
        public override string ToString()
        {
            return nazwa;
        }

        public void wykonaj_mail(string HTML)
        {
            //"<img src=" + node.GetAttributeValue("src", "") + " >"
            MailMessage wiadomosc = new MailMessage("platformy2015@gmail.com", mail_adress, "JTT", HTML);

            wiadomosc.IsBodyHtml = true;

            SmtpClient stmp = new SmtpClient();
            stmp.Port = 587;
            //stmp.DeliveryMethod = SmtpDeliveryMethod.Network;
            stmp.Host = "smtp.gmail.com";
            stmp.UseDefaultCredentials = false;
            stmp.Credentials = new System.Net.NetworkCredential("platformy2015@gmail.com", "platformy");
            //stmp.UseDefaultCredentials = false;
            stmp.EnableSsl = true;



            try
            {
                stmp.Send(wiadomosc);
            }
            catch (Exception ex)
            {

            }
        }

        public void wykonaj_Box(string HTML)
        {
            var strona = new Form2(HTML);
            strona.Show();
        }

    }

    public class Web : Data
    {

        
        public string strona_adres { get; set; }
        public string strona_tag { get; set; }
        public string errors { get; set; }

        public Web() { }
        public Web(string strona, string tag,string nazwa,string w)
        {
            
            this.strona_adres = strona;
            this.strona_tag = tag;
            this.nazwa = nazwa;
            //this.wykonanie = w;
            email = true;
            okno = false;
            mail_adress = w;
        }

        public Web(string strona, string tag, string nazwa)
        {
            
            this.strona_adres = strona;
            this.strona_tag = tag;
            this.nazwa = nazwa;
            //this.wykonanie = w;
            email = false;
            okno = true;
            mail_adress = null;
        }

        public string GetPageHtml(string uri)
        {
            using (WebClient wc = new WebClient())
            {
                byte[] data = wc.DownloadData(uri);
                string html = System.Net.WebUtility.HtmlDecode(Encoding.UTF8.GetString(data));
                return html;
            }
        }


        public override void wykonaj()
        {

            string trescstrony;
            byte[] tmp;




            HtmlAgilityPack.HtmlDocument strona = new HtmlAgilityPack.HtmlDocument();
            var html = GetPageHtml(strona_adres);
            strona.LoadHtml(html);


            foreach (HtmlAgilityPack.HtmlNode node in strona.DocumentNode.Descendants("img"))
            {
                //statusik.Text = "Parsowanie";
                //postęp.Value++;
                //this.Refresh();

                if (node.GetAttributeValue("alt", "").ToString().ToUpper().Contains(strona_tag.ToUpper()))
                {

                    
                    
                    if(email==true)
                    {
                        wykonaj_mail("<img src=" + node.GetAttributeValue("src", "") + " >");
                    }
                    else
                    {
                        wykonaj_Box("<img src=" + node.GetAttributeValue("src", "") + " >");
                    }

                }

            }
            
        }



    }

        public class Apik : Data
        {

            public string miasto { get; set; }

            public Apik() { }
            public override void wykonaj()
            {
                string json;
                using (WebClient wc = new WebClient())
                {
                    json = wc.DownloadString("http://api.openweathermap.org/data/2.5/weather?q="+miasto+",pl");
                }
                var weather = JsonConvert.DeserializeObject<WeatherObject>(json);

                if (email == false)
                {
                    wykonaj_Box("temperatura w miescie <b>" + miasto + "</b> wynosi:<b>" + (Convert.ToDouble(weather.main.temp) - 273).ToString() + "*C<\b>");
                }
                else
                {
                    wykonaj_mail("temperatura w miescie <b>" + miasto + "</b> wynosi:<b>" + (Convert.ToDouble(weather.main.temp) - 273).ToString() + "*C<\b>");
                }

            }
            public Apik(string m,string nazwa,string w)
            {
                this.miasto = m;
                this.nazwa = nazwa;
                //this.wykonanie = w;
                email = true;
                okno = false;
                mail_adress = w;
            }

            public Apik(string m, string nazwa)
            {
                this.miasto = m;
                this.nazwa = nazwa;
                //this.wykonanie = w;
                email = false;
                okno = true;
                mail_adress = null;
                

            }

        }

    public class Coord
    {
        public double lon { get; set; }
        public double lat { get; set; }
    }

    public class Sys
    {
        public double message { get; set; }
        public string country { get; set; }
        public int sunrise { get; set; }
        public int sunset { get; set; }
    }

    public class Weather
    {
        public int id { get; set; }
        public string main { get; set; }
        public string description { get; set; }
        public string icon { get; set; }
    }

    public class Main
    {
        public double temp { get; set; }
        public double temp_min { get; set; }
        public double temp_max { get; set; }
        public double pressure { get; set; }
        public double sea_level { get; set; }
        public double grnd_level { get; set; }
        public int humidity { get; set; }
    }

    public class Wind
    {
        public double speed { get; set; }
        public double deg { get; set; }
    }

    public class Clouds
    {
        public int all { get; set; }
    }

    public class WeatherObject
    {
        public Coord coord { get; set; }
        public Sys sys { get; set; }
        public List<Weather> weather { get; set; }
        public string @base { get; set; }
        public Main main { get; set; }
        public Wind wind { get; set; }
        public Clouds clouds { get; set; }
        public int dt { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public int cod { get; set; }
    }









}