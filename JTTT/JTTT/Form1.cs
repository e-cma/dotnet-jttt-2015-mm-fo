﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data.Entity;

namespace JTTT
{



    public partial class Form1 : Form
    {

        private BindingList<Data> NaszaLista = new BindingList<Data>();



        public Form1()
        {
            InitializeComponent();

            lista.DataSource = NaszaLista;


        }


        private void Wykonaj_Click(object sender, EventArgs e)
        {
            foreach (var tmp in NaszaLista)
            {
            
                tmp.wykonaj();
            
            }
            
            //string mail = mail_adres.Text;
            //string strona = strona_adres.Text;
            //string tag = strona_tag.Text;


                       



        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void dodawanie_Click(object sender, EventArgs e)
        {
            //ToTamto tmp = new ToTamto(mail_adres.Text, strona_adres.Text, strona_tag.Text, nazwa.Text);
            Data tmp;
            if(tabControl1.SelectedIndex==0)
            {

                if (tabControl2.SelectedIndex == 0)
                {

                    tmp = new Web(strona_adres.Text, strona_tag.Text, nazwa.Text,mail_adres.Text);
                }
                else
                {

                    tmp = new Web(strona_adres.Text, strona_tag.Text, nazwa.Text);
                }
            }
            else
            {
                if (tabControl2.SelectedIndex == 0)
                {

                    tmp = new Apik(miasto.Text, nazwa.Text, mail_adres.Text);
                }
                else
                {

                    tmp = new Apik(miasto.Text, nazwa.Text);
                }
            }



            NaszaLista.Add(tmp);

        }

        private void zapisz_Click(object sender, EventArgs e)
        {
            try{

                using(var db = new JTTContext2())
                {
                    var zapytanie = from b in db.Zadania orderby b.nazwa select b;
                    foreach(var odnosnik in zapytanie)
                    {
                        bool wystepuje = false;

                        foreach(var zadanie in NaszaLista)
                        {
                            if (zadanie.nazwa == odnosnik.nazwa) wystepuje = true;
                        }
                        if(wystepuje==false)
                        {
                            var delete = db.Zadania.Remove(odnosnik) ;
                            
                            
                        }
                    }
                    db.SaveChanges();


                    foreach (Data Zadania in NaszaLista)
                    {
                        bool wystepuje = false;
                        foreach (var odnosnik in zapytanie)
                        {
                            if (odnosnik.nazwa == Zadania.nazwa)
                            {
                                wystepuje = true;
                                break;
                            }
                        }
                        if(wystepuje==false)
                        {
                            var Zad = Zadania;
                            db.Zadania.Add(Zad);

                            db.SaveChanges();
                        }

                
                    }
            }
            }
            catch(Exception ex)
            {
                MessageBox.Show("blad/n"+ex.ToString());
            }
/*
                var zapytanie = from b in db.Zadania orderby b.nazwa select b;
                string str="";
                foreach(var item in zapytanie)
                {
                    str = str + " " + item.nazwa;

                }
                MessageBox.Show(str);
*/            
            
            
        }

        private void wczytaj_Click(object sender, EventArgs e)
        {
            
            try
            {             
                using(var db = new JTTContext2())
                {
                    
                    var zapytanie = from b in db.Zadania orderby b.nazwa select b;
                    NaszaLista.Clear();

                    foreach(var odnosnik in zapytanie)
                    {
                        NaszaLista.Add(odnosnik);
                    }
                }
            }

            catch(Exception ex)
            {
                MessageBox.Show("blad'  "+ex.ToString());
            }


        }

        private void usuń_Click(object sender, EventArgs e)
        {
            if(NaszaLista.Count>0)
            {
                int tmp = lista.SelectedIndex;
                NaszaLista.RemoveAt(tmp);
                
            }

        }


    }

    public class JTTContext2 : DbContext
    {
        public DbSet<Data> Zadania { get; set; }

    }

}
