﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Net;
using System.Net.Mail;

namespace JTTT
{

    public class Tasc
    {
        public int Id { get; set; }
        public virtual void wykonaj(string HTML){}

    }

    public class Mail : Tasc
    {
        public Mail() { }
        public string mail_adress { get; set; }
        public override void wykonaj(string HTML)
        {
            //"<img src=" + node.GetAttributeValue("src", "") + " >"
            MailMessage wiadomosc = new MailMessage("platformy2015@gmail.com", mail_adress, "JTT", HTML);

            wiadomosc.IsBodyHtml = true;

            SmtpClient stmp = new SmtpClient();
            stmp.Port = 587;
            //stmp.DeliveryMethod = SmtpDeliveryMethod.Network;
            stmp.Host = "smtp.gmail.com";
            stmp.UseDefaultCredentials = false;
            stmp.Credentials = new System.Net.NetworkCredential("platformy2015@gmail.com", "platformy");
            //stmp.UseDefaultCredentials = false;
            stmp.EnableSsl = true;



            try
            {
                stmp.Send(wiadomosc);
            }
            catch (Exception ex)
            {
                
            }
        }

        public Mail(string adres)
        {
            this.mail_adress = adres;
        }
    }

    public class Box : Tasc
    {
        public Box(){}

        public override void wykonaj(string HTML)
        {
            var strona=new Form2(HTML);
            strona.Show();
        }

    }

}
